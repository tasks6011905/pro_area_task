import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../extensions/extensions.dart';
import '../../store/user_state/user_bloc.dart';
import '../../themes/app_colors.dart';
import 'widgets/dashboard_footer.dart';
import 'widgets/user_content.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({
    Key? key,
  }) : super(key: key);

  @override
  State<DashboardPage> createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserBloc()..add(LoadUserEvent()),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'dashboardPage.title'.tr(),
          ),
          centerTitle: true,
        ),
        body: BlocBuilder<UserBloc, UserState>(
          builder: (context, state) {
            return SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: context.bodyHeight,
                ),
                child: IntrinsicHeight(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (state is UserLoadingState)
                        Center(
                          heightFactor: context.height / 100,
                          child: const CircularProgressIndicator(
                            color: AppColors.yellow,
                          ),
                        )
                      else if (state is UserLoadedState)
                        UserContent(
                          avatar: state.user.avatar,
                          fullName: state.user.fullName,
                          email: state.user.email,
                          phoneNumber: state.user.phoneNumber,
                        ),
                      if (state is UserErrorState)
                        Center(
                          child: Text(
                            state.error.toString(),
                          ),
                        ),
                      DashboardFooter(
                        getNewUser: getNewUser,
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  void getNewUser(BuildContext context) {
    BlocProvider.of<UserBloc>(context).add(LoadUserEvent());
  }
}
