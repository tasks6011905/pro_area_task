import 'package:flutter/material.dart';

import '../../../extensions/extensions.dart';
import '../../../providers/screen_service.dart';
import '../../../router.dart';

class DashboardFooter extends StatefulWidget {
  final Function(BuildContext) getNewUser;

  const DashboardFooter({
    Key? key,
    required this.getNewUser,
  }) : super(key: key);

  @override
  State<DashboardFooter> createState() => _DashboardFooterState();
}

class _DashboardFooterState extends State<DashboardFooter> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: () {
            widget.getNewUser(context);
          },
          child: Text(
            'dashboardPage.loadNewUser'.tr(),
            style: context.theme.button2.white,
          ),
        ).expandedHorizontally(),
        const SizedBox(height: 12),
        ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              Colors.green,
            ),
          ),
          child: Text(
            'dashboardPage.history'.tr(),
            style: context.theme.button2.white,
          ),
          onPressed: () {
            router.push(
              const UserHistoryRoute(),
            );
          },
        ).expandedHorizontally(),
        SizedBox(height: context.bottomPadding + 16),
      ],
    ).paddingHorizontal();
  }
}
