import 'package:flutter/material.dart';

import '../../mixins/after_layout_mixin.dart';
import '../../providers/screen_service.dart';
import '../../router.dart';
import 'widgets/splash_screen_logo.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage>
    with AfterLayoutMixin {
  @override
  Future afterFirstLayout(BuildContext context) async {
    await checkSession();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SplashScreenLogo(),
    );
  }

  Future<void> checkSession() async {
    await Future.delayed(
      const Duration(seconds: 3),
    );
    await router.pushAndPopAll(
      const DashboardRoute(),
    );
  }
}
