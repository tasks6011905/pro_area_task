import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import '../../../extensions/extensions.dart';
import '../../../utils/assets.dart';

class SplashScreenLogo extends StatelessWidget {
  const SplashScreenLogo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          ExtendedImage.asset(
            Assets.splashScreen,
            width: 200,
            height: 200,
          ),
          const SizedBox(height: 20),
          Text(
            'dashboardPage.splashScreenText'.tr(),
            style: context.theme.headline1.bold.darkGray,
          ),
        ],
      ),
    ).paddingHorizontal();
  }
}
