import 'package:flutter/material.dart';
import '../../../extensions/extensions.dart';

class HistoryButton extends StatelessWidget {
  final VoidCallback onTap;

  const HistoryButton({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: ElevatedButton(
        onPressed: onTap,
        child: Text(
          'userHistoryPage.deleteAll'.tr(),
        ),
      ).expandedHorizontally().paddingOnly(
            left: 24,
            right: 24,
            bottom: context.bottomPadding + 24,
          ),
    );
  }
}
