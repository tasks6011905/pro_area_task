import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../db/hive.dart';
import '../../http/repositories/auth_repository.dart';
import '../../models/user_model/user_model.dart';

part 'user_state.dart';

part 'user_event.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UserLoadingState()) {
    on<LoadUserEvent>(
      (event, emit) async {
        emit(UserLoadingState());
        try {
          final user = await AuthRepositories.getUser();
          UserDB.put(user);
          emit(UserLoadedState(user));
        } on DioError catch (error) {
          emit(UserErrorState(error.toString()));
        }
      },
    );
  }
}
